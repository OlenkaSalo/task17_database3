
 delimiter //
 create trigger after_insert_info
 after insert on employee for each row
 begin
  insert into info_copy(  new_surname, new_identity_number, new_passport,
                         action, timestamp  ,user )
               values(new.surname,new.identity_number, new.passport, 'insert', now(),user()) ;
               
 end //
 delimiter ; 
 
 
 delimiter //
 create trigger after_delete_info
 after delete on employee for each row
 begin
  insert into info_copy(  surname, identity_number, passport,
                         action, timestamp  ,user )
               values(old.surname, old.identity_number, old.passport, 'delete', now(),user()) ;
               
 end //
 delimiter ; 
 
  delimiter //
 create trigger after_update_info
 after update on employee for each row
 begin
  insert into info_copy( new_surname, surname, new_identity_number, identity_number, new_passport, passport,
                         action, timestamp  ,user )
               values(old.surname, old.identity_number, old.passport, new.surname,new.identity_number, new.passport, 'update', now(),user()) ;
               
 end //
 delimiter ; 