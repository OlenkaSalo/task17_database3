delimiter //
create trigger _before_insert_pharmacyID  
before insert on pharmacy_medicine  for each row
begin
 if(select count(*) from pharmacy where pharmacy.id = new.pharmacy_id) = 0 then
 SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = " Can't insert record. Foreign pharmacy  key doesn't exist";
 end if;
 end //
 delimiter ;
 
delimiter //
create trigger _before_update_pharmacyId 
before update on pharmacy_medicine for each row
begin
 if(select count(*) from pharmacy where pharmacy.id = new.pharmacy_id) = 0 then
 SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = " Can't update record. Foreign pharmacy  key doesn't exist";
 end if;
 end //
 delimiter ;
 
 delimiter //
 create trigger _before_update_pharmaceID
 before update on pharmacy for each row
 begin
 if(old.id <> new.id and (select count(*) from medicine_zone where pharmacy.id = old.id)<> 0 ) then
 SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t update record. Foreign key updates to medecine_zone table restricted!';
 end if;
 end //
 delimiter ; 
 
 delimiter //
 create trigger _before_delete_pharmacyID
 before delete on pharmacy for each row
 begin
 if(select count(*) from medicine where pharmacy.id = old.id <> 0 ) then
 SIGNAL SQLSTATE '45000' SET MYSQL_ERRNO = 30001, MESSAGE_TEXT = 'Can\'t delete record. FForeign key exists in medecine_zone table!';
 end if;
 end //
 delimiter ;
 