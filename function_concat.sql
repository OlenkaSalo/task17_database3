delimiter //
use storedpr_db //
drop function if exists joinName_street //
 create function joinName_street(valueId int)
 returns varchar(25)
 begin
 return(select concat(name, building_number) from pharmacy  where  id =valueId );
 end //
 
 delimiter ;
 
 select * ,joinName_street(pharmacy_id)  from  employee;